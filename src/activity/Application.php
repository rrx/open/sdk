<?php

namespace rrx\sdk\activity;

use Exception;
use rrx\sdk\RrxApi;

class Application extends RrxApi
{
    protected $config = [];

    private $endpointList = '/v1/activity/getList';    // 活动列表
    private $endpointDetail = '/v1/activity/get';    // 活动详情
    private $endpointVisit = '/v1/activity/getVisit';    // 活动访问量
    private $endpointPrize = '/v1/activityPrize/getList';   // 活动奖品列表
    private $endpointJoin = '/v2/activity/GetActivityJoin';   // 活动参与人数据接口
    private $endpointTop = '/v1/activity/GetActivityTop';   // 活动排行榜数据接口
    private $endpointOrder = '/v1/activity/GetActivityOrder';   // 活动中奖记录接口
    private $endpointPreset = '/v1/activity/GetActivityPreset';   // 活动报名记录接口
    private $endpointRecord = '/v1/activity/GetActivityRecord';   // 活动参与人详情数据接口
    private $endpointPlayer = '/v1/activity/GetActivityPlayer';   // 活动用户行为记录接口

    public function __construct($config)
    {
        parent::__construct($config);
        $this->config = $config;
    }

    /**
     * @throws Exception
     */
    public function getActivityList($page = 1, $limit = 20, $start_time = '', $end_time = '', $query_time_way = 1, $login_id = '',$delete_state=0)
    {
        if (empty($start_time)) {
            throw new Exception('start_time不能为空');
        }
        if (!is_string($start_time) || !strtotime($start_time)) {
            throw new Exception('start_time格式错误');
        }
        if (empty($end_time)) {
            throw new Exception('end_time不能为空');
        }
        if (!is_string($end_time) || !strtotime($end_time)) {
            throw new Exception('end_time格式错误');
        }

        $params = [
            'app_key' => $this->config['app_key'],
            'page' => $page,
            'limit' => $limit,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'query_time_way' => $query_time_way,
            'login_id' => $login_id,
            'delete_state' => $delete_state,
        ];
        return $this->sendRequest($this->endpointList, $params);
    }

    /**
     * 获取活动详情
     * @param $h5_guid
     * @return mixed
     * @throws Exception
     */
    public function getActivityDetail($h5_guid)
    {
        if (empty($h5_guid)) {
            throw new Exception('h5_guid不能为空');
        }

        $params = [
            'app_key' => $this->config['app_key'],
            'h5_guid' => $h5_guid,
        ];
        return $this->sendRequest($this->endpointDetail, $params);
    }

    /**
     * 获取活动访问量
     * @param $h5_guid
     * @return mixed
     * @throws Exception
     */
    public function getActivityVisit($h5_guid)
    {
        if (empty($h5_guid)) {
            throw new Exception('h5_guid不能为空');
        }

        $params = [
            'app_key' => $this->config['app_key'],
            'h5_guid' => $h5_guid,
        ];
        return $this->sendRequest($this->endpointVisit, $params);
    }

    /**
     * 获取活动奖品列表
     * @throws Exception
     */
    public function getActivityPrizeList($activity_guid)
    {
        return $this->commonGetList($this->endpointPrize, $activity_guid);
    }

    /**
     * 活动参与人数据
     * @param $activity_guid
     * @param string $h5_guid
     * @param int $page
     * @param int $limit
     * @return mixed
     * @throws Exception
     */
    public function getActivityJoin($activity_guid, $h5_guid = '', $page = 1, $limit = 20)
    {
        return $this->commonGetList($this->endpointJoin, $activity_guid, $h5_guid, $page, $limit);
    }

    /**
     * 获取活动排行榜
     * @param $activity_guid
     * @param $page
     * @param $limit
     * @param $h5_guid
     * @return mixed
     * @throws Exception
     */
    public function getActivityTop($activity_guid, $page = 1, $limit = 20, $h5_guid = '')
    {
        return $this->commonGetList($this->endpointTop, $activity_guid, $h5_guid, $page, $limit);
    }

    /**
     * 获取活动订单数据
     * @param $activity_guid
     * @param $h5_guid
     * @param $page
     * @param $limit
     * @param $start_time
     * @param $end_time
     * @return mixed
     * @throws Exception
     */
    public function getActivityOrder($activity_guid, $h5_guid = '', $page = 1, $limit = 20, $start_time = '', $end_time = '')
    {
        return $this->commonGetList($this->endpointOrder, $activity_guid, $h5_guid, $page, $limit, $start_time, $end_time);
    }

    /**
     * 获取活动报名记录
     * @param $activity_guid
     * @param $h5_guid
     * @param $page
     * @param $limit
     * @param $start_time
     * @param $end_time
     * @return mixed
     * @throws Exception
     */
    public function getActivityPreset($activity_guid, $h5_guid = '', $page = 1, $limit = 20, $start_time = '', $end_time = '')
    {
        return $this->commonGetList($this->endpointPreset, $activity_guid, $h5_guid, $page, $limit, $start_time, $end_time);
    }

    /**
     * 获取活动参与人详情数据
     * @param string $activity_guid
     * @param string $openid
     * @param string $h5_guid
     * @param int $page
     * @param int $limit
     * @param string $start_time
     * @param string $end_time
     * @return mixed
     * @throws Exception
     */
    public function getActivityRecord($activity_guid, $openid, $h5_guid = '', $page = 1, $limit = 20, $start_time = '', $end_time = '')
    {
        return $this->commonGetList($this->endpointRecord, $activity_guid, $h5_guid, $page, $limit, $start_time, $end_time, $openid);
    }

    /**
     * 获取活动用户行为记录
     * @param string $activity_guid
     * @param int $page
     * @param int $limit
     * @param string $create_time
     * @return mixed
     * @throws Exception
     */
    public function getActivityPlayer($activity_guid, $page = 1, $limit = 20, $create_time = '')
    {
        return $this->commonGetList($this->endpointPlayer, $activity_guid, '', $page, $limit, '', '', '', $create_time);
    }
}