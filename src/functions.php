<?php

/**
 * 获取毫秒时间戳
 * @return string
 */
if (!function_exists('getMicroTime')) {
    function getMicroTime()
    {
        list($msec, $sec) = explode(' ', microtime());
        return sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    }
}

/**
 * 格式化打印
 * @param $strOrArray
 */
if (!function_exists('dump')) {
    function dump($strOrArray)
    {
        echo '<pre>';
        var_dump($strOrArray);
        echo '</pre>';
    }
}