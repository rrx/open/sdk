<?php

namespace rrx\sdk\pay;

use Exception;
use rrx\sdk\Common;
use rrx\sdk\RrxApi;

class Application extends RrxApi
{
    protected $config = [];

    private $endpointPayResult = '/v1/pay/payresult';    // 短信状态通知接口

    public function __construct($config)
    {
        parent::__construct($config);
        $this->config = $config;
    }

    /**
     * @throws Exception
     */
    public function payNotify($openid, $order_no, $transaction_no = '')
    {
        if (empty($openid)) {
            throw new Exception('phone不能为空');
        }
        if (empty($order_no)) {
            throw new Exception('msg_id不能为空');
        }

        $params = [
            'app_key' => $this->config['app_key'],
            'openid' => $openid,
            'order_no' => $order_no,
            'transaction_no' => $transaction_no,
            'nonce_str' => Common::randomString(32)
        ];
        return $this->sendRequest($this->endpointPayResult, $params, 'POST');
    }
}