<?php

namespace rrx\sdk\tpl;

use Exception;
use rrx\sdk\RrxApi;

class Application extends RrxApi
{
    protected $config = [];

    private $endpointGetList = '/v1/tpl/getList';    // 团队模板列表
    private $endpointGetCategoryList = '/v1/tpl/getCategoryList';    // 删除活动

    public function __construct($config)
    {
        parent::__construct($config);
        $this->config = $config;
    }

    /**
     * @throws Exception
     */
     public function getList($category_id=0,$search_key='',$state=0,$type=0,$page=1,$limit=10)
     {
         $params = [
             'app_key' => $this->config['app_key'],
             'category_id'=> $category_id,
             'search_key' => $search_key,
             'state' => $state,
             'type' => $type,
             'page' => $page,
             'limit' => $limit
         ];
         return $this->sendRequest($this->endpointGetList, $params, 'post');
     }

     /**
      * @throws Exception
      */
     public function getCategoryList($category_id=0,$search_key='',$type=0,$page=1,$limit=10)
     {
         $params = [
             'app_key' => $this->config['app_key'],
             'category_id'=> $category_id,
             'search_key' => $search_key,
             'type' => $type,
             'page' => $page,
             'limit' => $limit
         ];
         return $this->sendRequest($this->endpointGetCategoryList, $params, 'post');
     }

}