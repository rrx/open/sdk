<?php

namespace rrx\sdk\relationShip;

use Exception;
use rrx\sdk\RrxApi;

class Application extends RrxApi
{
    protected $config = [];

    private $endpointRelationShip = '/v1/activity/GetActivityRelationShip';    // 活动传播关系链接口

    public function __construct($config)
    {
        parent::__construct($config);
        $this->config = $config;
    }

    /**
     * 获取活动参与人详情数据
     * @param string $h5_guid
     * @param string $share_openid
     * @param int $page
     * @param int $limit
     * @param string $start_time
     * @param string $end_time
     * @return mixed
     * @throws Exception
     */
    public function getActivityRelationShip($h5_guid, $share_openid = '', $page = 1, $limit = 20, $start_time = '', $end_time = '')
    {
        return $this->commonGetList($this->endpointRelationShip, '', $h5_guid, $page, $limit, $start_time, $end_time, '', $share_openid);
    }
}