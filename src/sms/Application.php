<?php

namespace rrx\sdk\sms;

use Exception;
use rrx\sdk\RrxApi;

class Application extends RrxApi
{
    protected $config = [];

    private $endpointSmsHook = '/v1/sms/smsHook';    // 短信状态通知接口

    public function __construct($config)
    {
        parent::__construct($config);
        $this->config = $config;
    }

    /**
     * @throws Exception
     */
    public function smsHook($phone, $msg_id)
    {
        if (empty($phone)) {
            throw new Exception('phone不能为空');
        }
        if (empty($msg_id)) {
            throw new Exception('msg_id不能为空');
        }

        $params = [
            'app_key' => $this->config['app_key'],
            'phone' => $phone,
            'msg_id' => $msg_id,
        ];
        return $this->sendRequest($this->endpointSmsHook, $params, 'POST', false);
    }
}