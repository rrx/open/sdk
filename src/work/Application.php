<?php

namespace rrx\sdk\work;

use Exception;
use rrx\sdk\RrxApi;

class Application extends RrxApi
{
    protected $config = [];

    private $endpointCreate = '/v1/work/create';    // 活动列表
    private $endpointDelete = '/v1/work/delete';    // 删除活动
    private $endpointCopy = '/v1/work/copy';    // 复制活动
    private $endpointEdit = '/v1/work/edit';    // 复制活动
    private $endpointState = '/v1/work/state';    // 复制活动
    private $endpointHistory = '/v1/work/history';    // 复制活动

    public function __construct($config)
    {
        parent::__construct($config);
        $this->config = $config;
    }

    /**
     * @throws Exception
     */
    public function createActivity($login_id = '', $type = 0, $tpl_team_guid = '', $share_info = '', $open_setting = '')
    {
        $params = [
            'app_key' => $this->config['app_key'],
            'tpl_team_guid' => $tpl_team_guid,
            'type' => $type,
            'login_id' => $login_id,
            'share_info' => is_array($share_info) ? json_encode($share_info) : $share_info,
            'open_setting' => is_array($open_setting) ? json_encode($open_setting) : $open_setting,
        ];
        return $this->sendRequest($this->endpointCreate, $params, 'post');
    }

     /**
      * @throws Exception
      */
      public function deleteActivity($login_id='',$h5_guid='')
      {
          $params = [
              'app_key' => $this->config['app_key'],
              'h5_guid' => $h5_guid,
              'login_id' => $login_id
          ];
          return $this->sendRequest($this->endpointDelete, $params, 'post');
      }

      /**
       * @throws Exception
       */
       public function copyActivity($login_id='',$h5_guid='')
       {
           $params = [
               'app_key' => $this->config['app_key'],
               'h5_guid' => $h5_guid,
               'login_id' => $login_id
           ];
           return $this->sendRequest($this->endpointCopy, $params, 'post');
       }

       /**
        * @throws Exception
        */
    public function editActivity($login_id = '', $h5_guid = '', $share_info = [], $open_setting = [])
    {
        $params = [
            'app_key' => $this->config['app_key'],
            'h5_guid' => $h5_guid,
            'login_id' => $login_id,
            'share_info' => is_array($share_info) ? json_encode($share_info) : $share_info,
            'open_setting' => is_array($open_setting) ? json_encode($open_setting) : $open_setting,
        ];
        return $this->sendRequest($this->endpointEdit, $params, 'post');
    }

        /**
         * @throws Exception
         */
         public function stateActivity($login_id='',$h5_guid='',$state=1)
         {
             $params = [
                 'app_key' => $this->config['app_key'],
                 'h5_guid' => $h5_guid,
                 'login_id' => $login_id,
                 'state' => $state
             ];
             return $this->sendRequest($this->endpointState, $params, 'post');
         }

         /**
          * @throws Exception
          */
          public function historyActivity($login_id='',$h5_guid='')
          {
              $params = [
                  'app_key' => $this->config['app_key'],
                  'h5_guid' => $h5_guid,
                  'login_id' => $login_id
              ];
              return $this->sendRequest($this->endpointHistory, $params, 'post');
          }
}