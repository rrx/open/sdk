<?php

namespace rrx\sdk;

/**
 * Class Factory.
 * @method static activity\Application activity(array $config) Create an instance of Activity\Application.
 * @method static sms\Application sms(array $config) Create an instance of Sms\Application.
 * @method static pay\Application pay(array $config) Create an instance of Pay\Application.
 * @method static relationShip\Application relationShip(array $config) Create an instance of RelationShip\Application.
 * @method static work\Application work(array $config) Create an instance of work\Application.
 */
class Factory
{
    /**
     * @param string $name
     */
    public static function make($name, array $config)
    {
        $application = "\\rrx\sdk\\{$name}\\Application";
        return new $application($config);
    }

    /**
     * 调用静态方法
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}