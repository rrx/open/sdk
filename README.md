# 人人秀SDK
支持php代码示例
## 目录
demo 接口演示

src 类库

## 安装
在项目根目录执行引用sdk
```
composer require rrx/sdk
```
![install.png](https://qn-assets.rrxiu.net/open/install.png)
## 使用
```
// 引用composer自动加载
require_once __DIR__ . '/vendor/autoload.php';

// 初始化人人秀活动数据sdk
$app = \rrx\sdk\Factory::activity($config);

// 获取活动参与人数据
$res = $app->getActivityJoin('hszkaeaa', '123456', 1, 20);
```


### 接口演示
* demo/activity   // 活动数据接口
* demo/actResult   // 活动结果通知接口
* demo/goods   // 第三方商品接口
* demo/login      // 第三方客户登录接口和系统集成免登录接口
* demo/lottery   // 第三方抽奖接口和指定用户中奖接口
* demo/pay   // 支付接口
* demo/points   // 积分接口
* demo/position   // 第三方位置接口
* demo/question   // 第三方题库接口
* demo/relationShip   // 活动传播关系链接口
* demo/sms   // 短信接口
* demo/tag   // 标签接口
* demo/task   // 第三方任务接口
* demo/thirdInfo   // 第三方信息查询接口
* demo/thirdPrize   // 第三方奖品接口
* demo/work   // 活动编辑接口
* demo/tpl   // 团队模板接口
