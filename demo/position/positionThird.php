<?php


require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$result = [
    'result' => 0,
    'msg' => 'ok',
];

$reqData = $_REQUEST;

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['h5_guid'] = '25c5rb';
        $reqData['redirect'] = 'http://192.168.28.200:1024/h/pages/activity/pz/home/mevcb0?guid=33f6c6a849e3bafe540ee54c8411fc6a&wsiteGuid=mevcb0';
        $reqData['openid'] = 'openid123456';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);

        $reqData['callback'] = 'callback1721093';
    }

    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    $result['msg'] = $e->getMessage();
    exit($reqData['callback'] . '(' . json_encode($result, JSON_UNESCAPED_UNICODE) . ')');
}

// 返回位置数据
$result['data'] = [
    'province' => '广东省',
    'city' => '深圳市',
    'county' => '南山区',
];
exit($reqData['callback'] . '(' . json_encode($result, JSON_UNESCAPED_UNICODE) . ')');