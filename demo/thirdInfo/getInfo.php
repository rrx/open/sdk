<?php

// 第三方信息查询

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$result = [
    'result' => 0,
    'msg' => 'ok',
    'data' => []
];

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['activity_guid'] = 'hszkaeaa';
        $reqData['h5_guid'] = '25c5rb';
        $reqData['openid'] = 'openid123456';
        $reqData['query_condition'] = '[{"key":"学号","value":"230232"},{"key":"姓名","value":"张三"}]';
        $reqData['field_list'] = '["学号","姓名","性别","语文","数学","英语"]';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }

    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    $result['result'] = 1;
    $result['msg'] = $e->getMessage();
    exit(json_encode($result, JSON_UNESCAPED_UNICODE));
}

// 返回数据
$result['data'] = [
    [
        '学号' => '230232',
        '姓名' => '张三',
        '性别' => '男',
        '语文' => '80',
        '数学' => '90',
        '英语' => '100',
    ],
    [
        '学号' => '230233',
        '姓名' => '李四',
        '性别' => '女',
        '语文'=> '90',
        '数学' => '80',
        '英语' => '90',
    ]
];
exit(json_encode($result, JSON_UNESCAPED_UNICODE));
