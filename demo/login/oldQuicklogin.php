<?php

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

// 老H5免登录

$oldQuickLoginApiUrl = 'https://api.rrx.cn/v1/user/login';

try {
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);
} catch (Exception $e) {
    exit($e->getMessage());
}

// 组织参数
$params = [
    'app_key' => $config['app_key'],                // 人人秀平台分配的AppKey
    'time_stamp' => getMicroTime(),                 // 1970-01-01开始的时间戳，精确到毫秒
    'account_id' => 'test_account',                 // 人人秀子账号的登录账号，不支持纯数字
];

// 签名并拼接参数key=value
$thirdLoginApiUri = $rrxApi->buildParamsUri($params);

// 拼接完整的人人秀免登录接口
$fullThirdLoginApiUrl = $oldQuickLoginApiUrl . '?' . $thirdLoginApiUri;

// 跳转到人人秀授权接口
header('Location: '. $fullThirdLoginApiUrl);
