<?php

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

// 免登录

$quickLoginApiUrl = 'http://192.168.28.89/www/webroot/api/v1/user/login';

// 初始化人人秀API
$rrxApi = new \rrx\sdk\RrxApi([
    'secret' => $config['secret'],   // 人人秀开放平台秘钥
]);

// 组织参数
$params = [
    'app_key' => $config['app_key'],    // 人人秀平台分配的AppKey
    'time_stamp' => getMicroTime(),                 // 1970-01-01开始的时间戳，精确到毫秒
    'account_id' => 'aks-802',                // 人人秀子账号的登录账号，不支持纯数字
    'nickname' => '',
    'mall_guid' => '',
];

// 签名并拼接参数key=value
$loginApiUri = $rrxApi->buildParamsUri($params);

// 拼接完整的人人秀免登录接口
$loginApiUrl = $quickLoginApiUrl . '?' . $loginApiUri;

var_dump($loginApiUri);
// 跳转到人人秀授权接口
header('Location: '. $loginApiUrl);
