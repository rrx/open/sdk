<?php

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$thirdLoginApiUrl = 'https://open.rrx.cn/api/v1/thirdAuth/login'; // 正式环境
//$thirdLoginApiUrl = 'http://192.168.28.200:81/www/webroot/api/v1/thirdAuth/login';  // 开发环境

try {
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $getData = $_GET;
    if (empty($getData) || !isset($getData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $getData['app_key'] = $config['app_key'];
        $getData['time_stamp'] = getMicroTime();
        $getData['h5_guid'] = 'mevcb0';
        $getData['share_openid'] = 'rrx123456789';
        $getData['redirect'] = 'http://192.168.28.200:1024/h/pages/activity/pz/home/mevcb0?guid=33f6c6a849e3bafe540ee54c8411fc6a&wsiteGuid=mevcb0';

        // 签名
        $getData['sign'] = $rrxApi->makeSign($getData);
    }

    // 验证签名
    $rrxApi->checkSign($getData);
} catch (Exception $e) {
    exit($e->getMessage());
}

// 模拟用户信息
$userInfo = [
    'openid'        => 'rrx123456789',
    'nickname'      => '人人秀第三方客户登录',
    'headimgurl'    => 'https://mfile.rrxiu.net/guest_head1.png',
];

// 组织参数
$params = [
    'app_key' => $getData['app_key'],               // 人人秀平台分配的AppKey
    'time_stamp' => getMicroTime(),                 // 1970-01-01开始的时间戳，精确到毫秒
    'openid' => $userInfo['openid'],                // 活动用户唯一标识 人人秀识别用户唯一性判断依据
    'nickname' => $userInfo['nickname'],            // 用户昵称
    'headimgurl' => $userInfo['headimgurl'],        // 用户头像地址
    'h5_guid' => $getData['h5_guid'],               // h5标记来自人人秀哪个活动
    'params' => '',                                 // 其他参数，json字符串对象
    'redirect' => $getData['redirect'],             // 当前活动地址，用于授权成功后跳转
    'fail_url' => '',                               // 授权失败，跳转地址
];

// 签名并拼接参数key=value
$thirdLoginApiUri = $rrxApi->buildParamsUri($params);

// 拼接完整的人人秀授权接口
$fullThirdLoginApiUrl = $thirdLoginApiUrl . '?' . $thirdLoginApiUri;

// 跳转到人人秀授权接口
header('Location: '. $fullThirdLoginApiUrl);