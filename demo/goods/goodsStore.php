<?php

// 第三方商品库

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$result = [
    'result' => 0,
    'msg' => 'ok',
    'data' => []
];

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['page'] = 1;
        $reqData['limit'] = 20;
        $reqData['login_id'] = '';
        $reqData['name'] = '';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }
    
    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    exit(json_encode($result, JSON_UNESCAPED_UNICODE));
}

// 返回商品数据
$result['data'] = [
    'total' => 2,
    'rows' => [
        [
            'goods_name' => '商品1',
            'goods_image' => '',
            'goods_price' => 100,
            'goods_code' => '10001',
        ],
        [
            'prize_name' => '商品2',
            'goods_image' => '',
            'goods_price' => 100,
            'goods_code' => '10001',
        ]
    ]
];
exit(json_encode($result, JSON_UNESCAPED_UNICODE));