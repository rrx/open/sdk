<?php

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$result = [
    'result' => 0,
    'msg' => 'ok',
    'data' => []
];

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['openid'] = 'openid123456';
        $reqData['order_no'] = 'order_no123456';
        $reqData['goods_code'] = '1001';
        $reqData['nickname'] = '';
        $reqData['headimgurl'] = '';
        $reqData['contacts'] = '';
        $reqData['phone'] = '';
        $reqData['h5_guid'] = '';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }

    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    exit(json_encode($result, JSON_UNESCAPED_UNICODE));
}

// 发奖逻辑...


// 返回中奖奖品数据
$result['data'] = [
    [
        'name' => '名称1',
        'value' => '名称值1',
    ],
    [
        'name' => '名称2',
        'value' => '名称值3',
    ],
];
$result['link'] = [
    'type' => 1,
    'url' => 'https://www.baidu.com',
    'button_text' => '点击查看商品',
];
exit(json_encode($result, JSON_UNESCAPED_UNICODE));