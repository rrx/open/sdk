<?php

// 活动结果通知

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

// 初始化人人秀API
$rrxApi = new \rrx\sdk\RrxApi([
    'secret' => $config['secret'],   // 人人秀开放平台秘钥
]);

$getData = $_GET;
if (empty($getData) || !isset($getData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
    $getData['app_key'] = $config['app_key'];
    $getData['time_stamp'] = getMicroTime();
    $getData['h5_guid'] = '25c5rb';
    $getData['activity_guid'] = 'hszkaeaa';
    $getData['openid'] = 'openId1234567890';
    $getData['employee_code'] = '';
    $getData['nickname'] = '小明';
    $getData['headimgurl'] = 'https://image.rrx.cn/avatar.png';
    $getData['event_name'] = '参与活动';
    $getData['event_source'] = 'H5活动';
    $getData['event_content'] = '答题得分';
    $getData['order_no'] = 'order_no123456';
    $getData['result'] = 0;
    $getData['msg'] = '';

    // 签名
    $getData['sign'] = $rrxApi->makeSign($getData);
}

// 验证签名
try {
    $rrxApi->checkSign($getData);
} catch (Exception $e) {
    exit($e->getMessage());
}

// 自己的业务逻辑


// 最后返回ok
echo 'ok';
