<?php

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$result = [
    'result' => 0,
    'msg' => 'ok',
    'data' => [],
];

// 验证签名
try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['data_type'] = '1';
        $reqData['question_id'] = '1001';
        $reqData['login_id'] = 'login_id123456';
        $reqData['page'] = '1';
        $reqData['limit'] = '20';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }

    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    $result['msg'] = $e->getMessage();
    exit(json_encode($result, JSON_UNESCAPED_UNICODE));
}

// 返回数据
if ($reqData['data_type'] == 1) {
    $result['data'] = [
        'total' => 2,
        'rows' => [
            [
                'id' => '1',
                'title' => '题库1',
                'question_count' => '2',
            ],
        ],
    ];
} else {
    $result['data'] = [
        'total' => 2,
        'rows' => [
            [
                'type' => 'text',
                'score' => '10',
                'question' => '你是谁？',
                'image' => '',
                'music' => '',
                'result' => [
                    [
                        'value' => '选项1',
                        'right' => true,
                    ],
                    [
                        'value' => '选项2',
                        'right' => false,
                    ],
                ],
            ],
        ],
    ];
}

exit(json_encode($result, JSON_UNESCAPED_UNICODE));