<?php

// 第三方任务

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

//$taskApiUrl = 'https://open.rrx.cn/api/v1/activityShare/completeTask'; // 正式环境
$taskApiUrl = 'http://192.168.28.200:81/www/webroot/api/v1/activityShare/completeTask'; // 测试环境

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $getData = $_GET;
    if (empty($getData) || !isset($getData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $getData['app_key'] = $config['app_key'];
        $getData['time_stamp'] = getMicroTime();
        $getData['activity_guid'] = 'hszkaeaa';
        $getData['h5_guid'] = '25c5rb';
        $getData['openid'] = 'openid123456';
        $getData['task_code'] = 'task01';
        $getData['redirect'] = 'http://192.168.28.200:1024/h/pages/activity/pz/home/mevcb0?guid=33f6c6a849e3bafe540ee54c8411fc6a&wsiteGuid=mevcb0';

        // 签名
        $getData['sign'] = $rrxApi->makeSign($getData);
    }

    // 验证签名
    $rrxApi->checkSign($getData);
} catch (Exception $e) {
    exit($e->getMessage());
}

// 任务逻辑。。。


// 人人秀任务

// 组织参数
$params = [
    'app_key' => $getData['app_key'],
    'time_stamp' => getMicroTime(),
    'activity_guid' => $getData['activity_guid'],
    'h5_guid' => $getData['h5_guid'],
    'openid' => $getData['openid'],
    'task_code' => $getData['task_code'],
    'reward_type' => 1,
    'add_number' => 1,
];

// 签名并拼接参数key=value
$taskApiUri = $rrxApi->buildParamsUri($params);

// 拼接完整的人人秀授权接口
$taskApiUrl = $taskApiUrl . '?' . $taskApiUri;

// 跳转到人人秀授权接口
header('Location: '. $taskApiUrl);