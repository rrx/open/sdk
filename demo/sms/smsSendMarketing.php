<?php

// 发送营销短信接口，模拟人人秀请求第三方开发者发送营销短信接口

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$result = [
    'result' => 0,
    'msg' => 'ok',
    'data' => []
];

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['phone'] = '13355556666';
        $reqData['msg'] = '1234';
        $reqData['uid'] = '123456';
        $reqData['login_id'] = '';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }

    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    $result['result'] = 1;
    $result['msg'] = $e->getMessage();
    exit(json_encode($result, JSON_UNESCAPED_UNICODE));
}

// 发送营销短信


// 发送营销短信后
$result['data'] = ['msg_id' => '1234'];
exit(json_encode($result, JSON_UNESCAPED_UNICODE));
