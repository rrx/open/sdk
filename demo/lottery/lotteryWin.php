<?php

// 指定用户中奖

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

$result = [
    'result' => 0,
    'msg' => 'ok',
    'data' => []
];

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['activity_guid'] = 'hszkaeaa';
        $reqData['h5_guid'] = '25c5rb';
        $reqData['openid'] = 'openid123456';
        $reqData['contacts'] = '[{"id":100,"title":"姓名","value":""},{"id":300,"title":"手机号","value":"15168372166"}]';
        $reqData['activity_prize'] = '[{"product_id":"18083","product_name":"商品1","product_type":"1","stock_qty":1}]';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }

    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    exit(json_encode($result, JSON_UNESCAPED_UNICODE));
}

// 返回数据
$result['data'] = [
    'win_type' => '1',
    'product_id' => '18083',
];
exit(json_encode($result, JSON_UNESCAPED_UNICODE));
