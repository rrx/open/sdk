<?php

// 客户标签更新接口，模拟人人秀请求第三方开发者接口

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['openid'] = 'rrx123456789';
        $reqData['tags'] = '["标签1","标签2"]';
        $reqData['result'] = 0;
        $reqData['msg'] = '';
        $reqData['params'] = '';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }

    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    exit($e->getMessage());
}

// 验证签名成功后根据自己的业务更新标签
$tags = json_decode($reqData['tags'], true);
dump($tags);

echo 'ok';  // 返回结果为 ok 字符串后，人人秀平台会标记通知成功，不再推送通知
