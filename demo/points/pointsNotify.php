<?php

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $reqData = $_REQUEST;
    if (empty($reqData) || !isset($reqData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $reqData['app_key'] = $config['app_key'];
        $reqData['time_stamp'] = getMicroTime();
        $reqData['order_no'] = 'rrx123456789';
        $reqData['result'] = 1;
        $reqData['msg'] = '库存不足';
        $reqData['params'] = '';

        // 签名
        $reqData['sign'] = $rrxApi->makeSign($reqData);
    }

    // 验证签名
    $rrxApi->checkSign($reqData);
} catch (Exception $e) {
    exit($e->getMessage());
}

// 根据自己的业务逻辑

// 如果兑换成功，开发者在系统内标记此订单为成功状态即可。

// 如果兑换失败，开发者需要将该订单标记为失败，并将之前减积分接口预扣的积分返还给用户。

echo 'ok';
