<?php

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

try {
    // 初始化人人秀API
    $rrxApi = new \rrx\sdk\RrxApi([
        'secret' => $config['secret'],   // 人人秀开放平台秘钥
    ]);

    $getData = $_GET;
    if (empty($getData) || !isset($getData['app_key'])) {  // 这里模拟一下人人秀平台携带的参数，正式环境中不需要
        $getData['app_key'] = $config['app_key'];
        $getData['time_stamp'] = getMicroTime();
        $getData['nonce_str'] = \rrx\sdk\Common::randomString(32);
        $getData['openid'] = 'openid123456';
        $getData['order_no'] = 'order_no123456';
        $getData['total_fee'] = 1;
        $getData['redirect'] = 'http://192.168.28.200:1024/h/pages/activity/pz/home/mevcb0?guid=33f6c6a849e3bafe540ee54c8411fc6a&wsiteGuid=mevcb0';
        $getData['fail_url'] = 'http://192.168.28.200:1024/h/pages/activity/pz/home/mevcb0?guid=33f6c6a849e3bafe540ee54c8411fc6a&wsiteGuid=mevcb0';
        $getData['contacts'] = '';
        $getData['phone'] = '';
        $getData['body'] = '';
        $getData['params'] = '';

        // 签名
        $getData['sign'] = $rrxApi->makeSign($getData);
    }

    // 验证签名
    $rrxApi->checkSign($getData);
} catch (Exception $e) {
    exit($e->getMessage());
}

// 支付逻辑

// 支付完成后跳转回人人秀活动页面$getData['redirect']