<?php

// 人人秀平台支付通知

require __DIR__ . '/../../vendor/autoload.php';

$config = require('./../config.php');

try {
    $app = \rrx\sdk\Factory::pay($config);
    $res = $app->payNotify('openid123456', 'order_no123456');
    dump($res);
} catch (Exception $e) {
    dump($e->getMessage());
}